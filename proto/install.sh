#!/usr/bin/sh

# Create directory, if it does not exist
mkdir -p /home/dpickem/Programs/arduino-1.5.5/libraries/home_automation

# Copy header file
cp home_automation_proto.pb.h /home/dpickem/Programs/arduino-1.5.5/libraries/home_automation/

# Copy source file
cp home_automation_proto.pb.c /home/dpickem/Programs/arduino-1.5.5/libraries/home_automation/
