/* Automatically generated nanopb constant definitions */
#include "home_automation_proto.pb.h"



const pb_field_t home_automation_msg_fields[6] = {
    {1, (pb_type_t) ((int) PB_HTYPE_REQUIRED | (int) PB_LTYPE_STRING),
    offsetof(home_automation_msg, type), 0,
    pb_membersize(home_automation_msg, type), 0, 0},

    {2, (pb_type_t) ((int) PB_HTYPE_REQUIRED | (int) PB_LTYPE_VARINT),
    pb_delta_end(home_automation_msg, id, type), 0,
    pb_membersize(home_automation_msg, id), 0, 0},

    {3, (pb_type_t) ((int) PB_HTYPE_REQUIRED | (int) PB_LTYPE_VARINT),
    pb_delta_end(home_automation_msg, lightLevel, id), 0,
    pb_membersize(home_automation_msg, lightLevel), 0, 0},

    {4, (pb_type_t) ((int) PB_HTYPE_REQUIRED | (int) PB_LTYPE_FIXED32),
    pb_delta_end(home_automation_msg, current, lightLevel), 0,
    pb_membersize(home_automation_msg, current), 0, 0},

    {5, (pb_type_t) ((int) PB_HTYPE_REQUIRED | (int) PB_LTYPE_FIXED32),
    pb_delta_end(home_automation_msg, temp, current), 0,
    pb_membersize(home_automation_msg, temp), 0, 0},

    PB_LAST_FIELD
};

