/* Automatically generated nanopb header */
#ifndef _PB_HOME_AUTOMATION_PROTO_PB_H_
#define _PB_HOME_AUTOMATION_PROTO_PB_H_
#include <pb.h>


/* Enum definitions */
/* Struct definitions */
typedef struct {
    char type[3];
    int32_t id;
    int32_t lightLevel;
    float current;
    float temp;
} home_automation_msg;

/* Default values for struct fields */

/* Struct field encoding specification for nanopb */
extern const pb_field_t home_automation_msg_fields[6];

#endif
