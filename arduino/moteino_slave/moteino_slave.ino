// Sample RFM69 sender/node sketch, with ACK and optional encryption
// Sends periodic messages of increasing length to gateway (id=1)
// It also looks for an onboard FLASH chip, if present
// Library and code by Felix Rusu - felix@lowpowerlab.com
// Get the RFM69 and SPIFlash library at: https://github.com/LowPowerLab/

#include <RFM69.h>
#include <SPI.h>

#define NODEID        99   //unique for each node on same network --> get assigned from master node
#define NETWORKID     100  //the same on all nodes that talk to each other
#define GATEWAYID     1

//Match frequency to the hardware version of the radio on your Moteino (uncomment one):
#define FREQUENCY       RF69_915MHZ       /* #define FREQUENCY   RF69_433MHZ OR #define FREQUENCY   RF69_868MHZ */
#define ENCRYPTKEY      "daniels&home"    /* exactly the same 16 characters/bytes on all nodes! */
#define ACK_TIME        30                /* max # of ms to wait for an ack */
#define LED             9                 /* Moteinos have LEDs on D9 */
#define SERIAL_BAUD     115200
#define TRANSMITPERIOD  3000              /* transmit a packet to gateway so often (in ms) */
#define RETRIES         3                 /* How often will a transmission be retried */
#define RETRY_WAIT_TIME 15                /* Wait time between retries */

/* Define message types */
#define MSG_ID_REQUEST  0
#define MSG_ID_GRANTED  1
#define MSG_SET_LIGHT   2
#define MSG_DATA        3

long lastSent        = millis();
bool promiscuousMode = true; /* set to 'true' to sniff all packets on the same network */
bool requestACK   = true;
bool initialized = false;
RFM69 radio;

/* Define the node ID variable that can be reassigned */
byte nodeID = NODEID;

/* Define pins */
int lightPin   = 6;
int tempPin    = A0;
int currentPin = A1;

/* Define payload struct */
typedef struct {		
  byte  nodeId;      /* store this nodeId */
  float current;     /* current consumption in amps               SLAVE -> MASTER*/
  float temp;        /* temperature in centigrade                 SLAVE -> MASTER*/
  int   lightLevel;  /* desired light level sent by master node   MASTER -> SLAVE */
  int   msgType;     /* indicate what type of message is being sent */
} Payload;
Payload data;

/* Define variables to hold current sensor readings */
int lightLevel = 0;       /* Store current light level */
float temp     = 40.0;    /* Initial estimate of temperature */
float current  = 0.4;     /* Initial estimate of current */

void setup() {
  /* Initialize serial and radio */
  Serial.begin(SERIAL_BAUD);
  radio.initialize(FREQUENCY, nodeID, NETWORKID);
  
  /* Enable hardware AES encryption */
  radio.encrypt(ENCRYPTKEY);
  
  /* Enable pins as input or output */
  pinMode(lightPin, OUTPUT); 
  
  Serial.print("Transmitting at "); Serial.print(FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915); Serial.println("  Mhz...");
  Serial.println("Requesting valid ID from Master node...");
  
  /* Request ID from master node */
  //sendMessage(nodeID, 0.0, 0.0, 0, MSG_ID_REQUEST);   
}

void loop() {
  /* --------------------------------- */
  /*         RECEIVE DATA              */
  /* --------------------------------- */
  if (radio.receiveDone()) {
    Serial.print('[');Serial.print(radio.SENDERID, DEC);Serial.print("] ");
    Serial.print(" [RX_RSSI:");Serial.print(radio.readRSSI());Serial.print("]");
    
    if (promiscuousMode) {
      Serial.print("to [");Serial.print(radio.TARGETID, DEC);Serial.print("] ");
    }
    
    /* Verify message length */
    if (radio.DATALEN != sizeof(Payload))
      Serial.print("Invalid payload received, not matching Payload struct!");
    else {
      /* Parse data - assume radio.DATA actually contains our struct and not something else */
      data = *(Payload*)radio.DATA; 
      Serial.print(" nodeId = ");     Serial.print(data.nodeId);
      Serial.print(" current = ");    Serial.print(data.current);
      Serial.print(" temp = ");       Serial.print(data.temp);
      Serial.print(" lightlevel = "); Serial.print(data.lightLevel);
      Serial.print(" msgType = ");    Serial.print(data.msgType);
      
      /* Process message */
      processMessage();
    }
    
    if (radio.ACK_REQUESTED) {
      byte theNodeID = radio.SENDERID;
      radio.sendACK();
      Serial.println(" - ACK sent.");
    } else {
      Serial.println();
    }
  }
  
  /* --------------------------------- */
  /*         SEND DATA EVERY 30s       */
  /* --------------------------------- */
  if (millis() - lastSent > TRANSMITPERIOD) {  
    /* Send message, if no valid ID has been assigned, request one */
    if(!initialized) {
      sendMessage(nodeID, 0.0, 0.0, 0, MSG_ID_REQUEST);
    } else {
      sendMessage(nodeID, current, temp, lightLevel, MSG_DATA);  
    }

    /* Update time of last sending */
    lastSent = millis();
  }
  
  /* ------------------------------------------ */
  /*  READ SENSOR DATA EVERY AT LOOP FREQ       */
  /* ------------------------------------------ */
  /* Keep running averages of sensor values */
  float alpha = 0.01;
  temp    = (1 - alpha) * temp + alpha * getTemperature(tempPin);
  current = (1 - alpha) * current + alpha * getCurrent(currentPin);
}

/* ------------------------------------------ */
/*            UTILITY FUNCTIONS               */
/* ------------------------------------------ */
float getVoltage(int pin) {
  return (analogRead(pin) * 0.00322265625); /* converting from a 0 to 1024 digital range
                                             * to 0 to 3.3 volts (each 1 tick equals ~ 3.2 millivolts
                                             */
}

float getTemperature(int pin) {
  return (getVoltage(pin) - 0.5) * 100;  /* converting from 10 mv per degree wit 500 mV offset
                                          * to degrees ((volatge - 500mV) times 100)
                                          */
}

float getCurrent(int pin) {
  // TODO: implement this function 
  return 0.5;
}

bool sendMessage(int nodeID, float current, float temp, int lightLevel, int msgType) {
  /* Declare message */
  Payload tmpMsg;

  /* Fill in the struct with new values */
  tmpMsg.nodeId     = nodeID;
  tmpMsg.current    = current;
  tmpMsg.temp       = temp;
  tmpMsg.lightLevel = lightLevel;
  tmpMsg.msgType    = msgType;
    
  /* Send data if channel is not occupied */
  //Serial.print("Sending struct ("); Serial.print(sizeof(data)); Serial.print(" bytes) ... ");
  if(radio.canSend()) {
    if (radio.sendWithRetry(GATEWAYID, (const void*)(&tmpMsg), sizeof(tmpMsg), RETRIES, RETRY_WAIT_TIME)) {
      Serial.println("sendMessage: Message sent successfully.");
      Serial.print("Node ID: "); Serial.println(nodeID);
      return true;
    } else {
      Serial.println("sendMessage: ERROR, no ACK received ...");
      return false;
    }
  }
}

void processMessage() {
  switch(data.msgType) {
    case MSG_ID_REQUEST:
      /* Nothing to be done, since slaves can't process ID requests, only the master node can */
      break;
    case MSG_ID_GRANTED:
      initialized = true;
      nodeID = data.lightLevel;    /* Reassign node ID received from master node */
      radio.setAddress(nodeID);    /* Update RFM69W register */
      Serial.print("ID request granted. Updated ID to "); Serial.print(nodeID); Serial.println(".");
      break;
    case MSG_SET_LIGHT:
      /* Set desired brightness level of light */
      lightLevel = data.lightLevel;
      analogWrite(lightPin, lightLevel);
      break;
    case MSG_DATA:
      sendMessage(nodeID, current, temp, lightLevel, MSG_DATA);
      break;
    default:
      break;
  }
}
