/* This sketch enables an Arduino to receive NanoPb Legobotics 
 * messages over the serial line and forward it over an XBee
 * connected to Pin 2 and 3 via SoftwareSerial
 */

/* Include standard libraries */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int relayPIN = 12;
int tempPin = 0 ;
int photocellPin1 = 1;
int statusPin = 13;

int temp;
int lightLevel1;
int dat;

int timer = 0;
  
void setup() {
  Serial.begin(9600);
  Serial.println("Home Automation NanoPb Receiver started.");
  
  pinMode(relayPIN, OUTPUT);
  pinMode(statusPin, OUTPUT);
  
  digitalWrite(statusPin, LOW);
  digitalWrite(relayPIN, LOW);
  delay(500);
  digitalWrite(statusPin, HIGH);
  digitalWrite(relayPIN, HIGH);
  delay(1000);
  digitalWrite(statusPin, LOW);
  digitalWrite(relayPIN, LOW);
}

void loop() {
  digitalWrite(statusPin, LOW);
  digitalWrite(relayPIN, LOW);
  
  temp        = analogRead(tempPin);
  lightLevel1 = analogRead(photocellPin1);
  dat         = (125*temp)>>8 ; // Temperature calculation formula

  Serial.print("Temp / Light Level : ") ; //print “Tep” means temperature
  Serial.print(dat) ; // print the value of dat
  Serial.print("C / ");
  Serial.println(lightLevel1);
  
  /* Send data every 1 second */  
  delay(500);
  digitalWrite(statusPin, HIGH);
  digitalWrite(relayPIN, HIGH);
  delay(500);
}
