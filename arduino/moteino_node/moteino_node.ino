// Sample RFM69 sender/node sketch, with ACK and optional encryption
// Sends periodic messages of increasing length to gateway (id=1)
// It also looks for an onboard FLASH chip, if present
// Library and code by Felix Rusu - felix@lowpowerlab.com
// Get the RFM69 and SPIFlash library at: https://github.com/LowPowerLab/

#include <RFM69.h>
#include <SPI.h>

#define NODEID        2    //unique for each node on same network
#define NETWORKID     100  //the same on all nodes that talk to each other
#define GATEWAYID     1

//Match frequency to the hardware version of the radio on your Moteino (uncomment one):
#define FREQUENCY     RF69_915MHZ         // #define FREQUENCY   RF69_433MHZ OR #define FREQUENCY   RF69_868MHZ
#define ENCRYPTKEY    "daniels&home"      //exactly the same 16 characters/bytes on all nodes!
#define ACK_TIME      30                  // max # of ms to wait for an ack
#define LED           9                   // Moteinos have LEDs on D9
#define SERIAL_BAUD   115200
#define TRANSMITPERIOD 3000;             //transmit a packet to gateway so often (in ms)

bool promiscuousMode = false; /* set to 'true' to sniff all packets on the same network */
boolean requestACK   = true;
RFM69 radio;

/* Define pins */
int lightPin   = 6;
int tempPin    = A0;
int currentPin = A1;

/* Define payload struct */
typedef struct {		
  int   nodeId;      /* store this nodeId */
  float current;     /* current consumption in amps               SLAVE -> MASTER*/
  float temp;        /* temperature in centigrade                 SLAVE -> MASTER*/
  int   lightLevel;  /* desired light level sent by master node   MASTER -> SLAVE */
} Payload;
Payload data;

void setup() {
  /* Initialize serial and radio */
  Serial.begin(SERIAL_BAUD);
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  
  /* Enable hardware AES encryption */
  radio.encrypt(ENCRYPTKEY);
  
  /* Enable pins as input or output */
  pinMode(lightPin, OUTPUT); 
  
  Serial.print("Transmitting at "); Serial.print(FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915); Serial.println("  Mhz...");
}

long lastPeriod = -1;
void loop() {
  /* --------------------------------- */
  /*         RECEIVE DATA              */
  /* --------------------------------- */
  if (radio.receiveDone())
  {
    Serial.print('[');Serial.print(radio.SENDERID, DEC);Serial.print("] ");
    Serial.print(" [RX_RSSI:");Serial.print(radio.readRSSI());Serial.print("]");
    if (promiscuousMode) {
      Serial.print("to [");Serial.print(radio.TARGETID, DEC);Serial.print("] ");
    }
    
    /* Verify message length */
    if (radio.DATALEN != sizeof(Payload))
      Serial.print("Invalid payload received, not matching Payload struct!");
    else {
      /* Parse data - assume radio.DATA actually contains our struct and not something else */
      data = *(Payload*)radio.DATA; 
      Serial.print(" nodeId = ");     Serial.print(data.nodeId);
      Serial.print(" current = ");    Serial.print(data.current);
      Serial.print(" temp = ");       Serial.print(data.temp);
      Serial.print(" lightlevel = "); Serial.print(data.lightLevel);
      
      /* Set desired brightness level of light */
      analogWrite(lightPin, data.lightLevel);
    }
    
    if (radio.ACK_REQUESTED) {
      byte theNodeID = radio.SENDERID;
      radio.sendACK();
      Serial.println(" - ACK sent.");
    }
    
    Blink(LED,3);
  }
  
  /* --------------------------------- */
  /*         SEND DATA EVERY 30s       */
  /* --------------------------------- */
  int currPeriod = millis()/TRANSMITPERIOD;
  if (currPeriod != lastPeriod) {
    /* fill in the struct with new values */
    data.nodeId     = NODEID;
    data.current    = 0.5;
    data.temp       = 91.23;
    data.lightLevel = 255;
    
    /* Send data */
    Serial.print("Sending struct ("); Serial.print(sizeof(data)); Serial.print(" bytes) ... ");
    if (radio.sendWithRetry(GATEWAYID, (const void*)(&data), sizeof(data))) {
      Serial.println(" OK!");
    } else {
      Serial.println(" ERROR, no ACK received ...");
    }
    
    Blink(LED,3);
    lastPeriod=currPeriod;
  }
}

void Blink(byte PIN, int DELAY_MS) {
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN,HIGH);
  delay(DELAY_MS);
  digitalWrite(PIN,LOW);
}

///* --------------------------------- */
///*         RECEIVE DATA  OLD         */
///* --------------------------------- */
//
//if (radio.receiveDone())  {
//  char receiveBuff[20];
//  Serial.print('['); Serial.print(radio.SENDERID, DEC); Serial.print("] ");
//  
//  /* Locally buffer data */
//  for (byte i = 0; i < radio.DATALEN; i++) {
//    receiveBuff[i] = radio.DATA[i];
//    Serial.print((char)radio.DATA[i]);
//  }
//  
//  Serial.print("   [RX_RSSI:"); Serial.print(radio.RSSI); Serial.print("]");
//
//  /* Parse data - this assumes the message is only a single value, the desired lamp brightness */
//  int brightness = atoi(receiveBuff);   //atof(receiveBuff);
//  
//  /* Set brightness level of light */
//  analogWrite(lightPin, brightness);
//  
//  /* Send ACK if requested */
//  if (radio.ACK_REQUESTED) {
//    radio.sendACK();
//    delay(10);
//  }
//  
//  Blink(LED, 5);
//  Serial.println();
//}

///* --------------------------------- */
///*         SEND DATA EVERY 30s OLD   */
///* --------------------------------- */
//int currPeriod = millis()/TRANSMITPERIOD;
//if (currPeriod != lastPeriod) {
//  /* create and send data message */
//  char sendBuff[50];
//  float temp    = 1.134;
//  float current = 0.450;
//
//  sprintf(sendBuff, "%d, %d", int(temp*1000), int(current*1000));    /* Send data as integers */
//  byte buffLen=strlen(sendBuff);
//  radio.sendWithRetry(GATEWAYID, sendBuff, buffLen);
//  
//  /* Update last send period */    
//  lastPeriod = currPeriod;
//
//  /* Blink when message is sent */
//  Blink(LED,3);
//}
