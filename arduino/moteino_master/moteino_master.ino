// Sample RFM69 sender/node sketch, with ACK and optional encryption
// Sends periodic messages of increasing length to gateway (id=1)
// It also looks for an onboard FLASH chip, if present
// Library and code by Felix Rusu - felix@lowpowerlab.com
// Get the RFM69 and SPIFlash library at: https://github.com/LowPowerLab/

#include <RFM69.h>
#include <SPI.h>

#define NODEID         1    //unique for each node on same network
#define NETWORKID      100  //the same on all nodes that talk to each other
#define GATEWAYID      1
#define MAXNODES       100

//Match frequency to the hardware version of the radio on your Moteino (uncomment one):
#define FREQUENCY       RF69_915MHZ         // #define FREQUENCY   RF69_433MHZ OR #define FREQUENCY   RF69_868MHZ
#define ENCRYPTKEY      "daniels&home"      //exactly the same 16 characters/bytes on all nodes!
#define ACK_TIME        30                  // max # of ms to wait for an ack
#define LED             9                   // Moteinos have LEDs on D9
#define SERIAL_BAUD     115200
#define TRANSMITPERIOD  1000              //transmit a packet to gateway so often (in ms)
#define RETRIES         3                 /* How often will a transmission be retried */
#define RETRY_WAIT_TIME 15                /* Wait time between retries */

/* Define message types */
#define MSG_ID_REQUEST  0
#define MSG_ID_GRANTED  1
#define MSG_SET_LIGHT   2
#define MSG_DATA        3

long lastSent        = millis();
bool promiscuousMode = true; /* set to 'true' to sniff all packets on the same network */
bool requestACK   = true;
RFM69 radio;

/* Define structure to hold all currently registered nodes */
byte currentNodes[100];

/* Define payload struct */
typedef struct {		
  byte  nodeId;      /* store this nodeId */
  float current;     /* current consumption in amps               SLAVE -> MASTER*/
  float temp;        /* temperature in centigrade                 SLAVE -> MASTER*/
  int   lightLevel;  /* desired light level sent by master node   MASTER -> SLAVE */
  int   msgType;     /* indicate what type of message is being sent */
} Payload;
Payload data;

void setup() {
  /* Initialize serial and radio */
  Serial.begin(SERIAL_BAUD);
  radio.initialize(FREQUENCY, NODEID, NETWORKID);
  
  /* Enable hardware AES encryption */
  radio.encrypt(ENCRYPTKEY);
  
  /* Initialize current node array */
  for (int i = 0; i < MAXNODES; i++) {
    currentNodes[i] = 0;
  }
  
  Serial.print("Transmitting at "); Serial.print(FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915); Serial.println("  Mhz...");
}

void loop() {
  /* --------------------------------- */
  /*         RECEIVE DATA              */
  /* --------------------------------- */
  if (radio.receiveDone()) {
    Serial.print('[');Serial.print(radio.SENDERID, DEC);Serial.print("] ");
    Serial.print(" [RX_RSSI:");Serial.print(radio.readRSSI());Serial.print("]");
    if (promiscuousMode) {
      Serial.print("to [");Serial.print(radio.TARGETID, DEC);Serial.print("] ");
    }
    
    /* Verify message length */
    if (radio.DATALEN != sizeof(Payload))
      Serial.print("Invalid payload received, not matching Payload struct!");
    else {
      /* Parse data - assume radio.DATA actually contains our struct and not something else */
      data = *(Payload*)radio.DATA; 
      Serial.print(" nodeId = ");     Serial.print(data.nodeId);
      Serial.print(" current = ");    Serial.print(data.current);
      Serial.print(" temp = ");       Serial.print(data.temp);
      Serial.print(" lightlevel = "); Serial.print(data.lightLevel);
      Serial.print(" msgType = ");    Serial.print(data.msgType);
    }
    
    if (radio.ACK_REQUESTED) {
      radio.sendACK();
      Serial.println(" - ACK sent.");
    } else {
      Serial.println();
    }
    
    /* Process message */
    processMessage();
  }
  
  /* --------------------------------- */
  /*         SEND DATA EVERY 1s        */
  /* --------------------------------- */
  if (millis() - lastSent > TRANSMITPERIOD) { 
    /* Send message to all registered nodes */
    for (int i = 0; i < MAXNODES; i++) {
      if(currentNodes[i] > 0) {
        //sendMessage(currentNodes[i], NODEID, -1, -1, 255, MSG_DATA);
        //sendMessage(99, NODEID, -1, -1, 255, MSG_DATA);
      }
    }
    
    sendMessage(99, NODEID, 0.0, 0.0, 4, MSG_ID_GRANTED);
    sendMessage(99, NODEID, 0.0, 0.0, 4, MSG_ID_GRANTED);
    sendMessage(4, NODEID, 0.0, 0.0, 16, MSG_DATA);
    
    /* Update time of last sending */
    lastSent = millis();
  }
}

void Blink(byte PIN, int DELAY_MS) {
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN,HIGH);
  delay(DELAY_MS);
  digitalWrite(PIN,LOW);
}

bool sendMessage(byte dest, int nodeID, float current, float temp, int lightLevel, int msgType) {
  /* Declare message */
  Payload tmpMsg;

  /* Fill in the struct with new values */
  tmpMsg.nodeId     = nodeID;
  tmpMsg.current    = current;
  tmpMsg.temp       = temp;
  tmpMsg.lightLevel = lightLevel;
  tmpMsg.msgType    = msgType;
  
  Serial.print("sent nodeId = "); Serial.print(tmpMsg.nodeId);
  Serial.print(" current = ");    Serial.print(tmpMsg.current);
  Serial.print(" temp = ");       Serial.print(tmpMsg.temp);
  Serial.print(" lightlevel = "); Serial.print(tmpMsg.lightLevel);
  Serial.print(" msgType = ");    Serial.print(tmpMsg.msgType);
  Serial.print(" dest = ");       Serial.println(dest);
    
  /* Send data if channel is not occupied */
  //Serial.print("Sending struct ("); Serial.print(sizeof(data)); Serial.print(" bytes) ... ");
  if(radio.canSend()) {
    if (radio.sendWithRetry(dest, (const void*)(&tmpMsg), sizeof(tmpMsg), RETRIES, RETRY_WAIT_TIME)) {
      Serial.println("sendMessage: Message sent successfully.");
      return true;
    } else {
      Serial.println("sendMessage: ERROR, no ACK received ...");
      return false;
    }
  }
}

void processMessage() {
  byte newID = 0;
  
  switch(data.msgType) {
    case MSG_ID_REQUEST:
      /* Determine first unused ID, ID 0 and 1 are reserved */
      for (byte i = 2; i < MAXNODES; i++) {
        if(currentNodes[i] == 0) {
          currentNodes[i] = i;
          newID = i;
          break;
        }
      }
      
      Serial.print("MSG_ID_GRANTED received. Assigning ID "); Serial.println(newID);
      Serial.print("Sending data to node: "); Serial.println(data.nodeId);
      sendMessage(data.nodeId, NODEID, 0.0, 0.0, newID, MSG_ID_GRANTED);
      break;
    case MSG_ID_GRANTED:
      /* Nothing to be done here, since the master never updates its own ID */
      break;
    case MSG_SET_LIGHT:
      /* Nothing to be done here, since the master does not set a lightLevel */
      break;
    case MSG_DATA:
      /* Nothing to be done here, since the master does not have sensors */
      break;
    default:
      break;
  }
}
