/* This sketch enables an Arduino to receive NanoPb Legobotics 
 * messages over the serial line and forward it over an XBee
 * connected to Pin 2 and 3 via SoftwareSerial
 */

/* Include standard libraries */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Include software serial library */
#include "SoftwareSerial.h"

/* Include nanopb interprocess communication */
#include <pb.h>
#include <pb_encode.h>
#include <pb_decode.h>

/* Include custom messages */
#include <home_automation_proto.pb.h>

// * RX is digital pin 2 (connect to TX of other device)
// * TX is digital pin 3 (connect to RX of other device)
SoftwareSerial mySerial(2,3); // RX, TX, works on nano

int statusPIN1 = 9;
int statusPIN2 = 10;
int statusPIN3 = 11;
int relayPIN = 12;

int tempPin = 0 ;
int photocellPin1 = 1;
int photocellPin2 = 2;
int photocellPin3 = 3;

bool xbee = true;

uint8_t in_buffer[256];
uint8_t out_buffer[256];

int temp;
int lightLevel1;
int lightLevel2;
int lightLevel3;
int dat;

int timer = 0;
  
void setup() {
  Serial.begin(9600);
  Serial.println("Home Automation NanoPb Receiver started.");
  
  /* Set up soft serial */
  mySerial.begin(9600);
  
  pinMode(statusPIN1, OUTPUT);
  pinMode(statusPIN2, OUTPUT);
  pinMode(statusPIN3, OUTPUT);
  pinMode(relayPIN, OUTPUT);
  
  digitalWrite(statusPIN1, LOW);
  digitalWrite(relayPIN, LOW);
  delay(500);
  digitalWrite(statusPIN1, HIGH);
  digitalWrite(relayPIN, HIGH);
  delay(1000);
  digitalWrite(statusPIN1, LOW);
  digitalWrite(relayPIN, LOW);
}

void loop() {
  digitalWrite(statusPIN1, LOW);
  digitalWrite(statusPIN2, LOW);
  digitalWrite(statusPIN3, LOW);
  
  /* TODO: for now read from serial line directly hooked up from computer 
   *       --> change to XBee once debugging is done!!!
   */
  if(mySerial.available()) {
    /* Read from software serial, since that is where the thermometer data is coming from */
    int packet_size = mySerial.read();
    int bytes_read = mySerial.readBytes((char*)in_buffer, packet_size);
    home_automation_msg cmd_msg;

    /* -------------------------------------------
     * Check if we read the right number of bytes
     * -------------------------------------------
     */
    if (bytes_read != packet_size) {
      /* ---------------------------------------------
       * ERROR: Wrong number of packets read
       * ---------------------------------------------
       */
      sendDataMessage(-1);
      digitalWrite(statusPIN1, HIGH);
    } else {
      pb_istream_t istream = pb_istream_from_buffer(in_buffer, bytes_read);
  
      digitalWrite(statusPIN2, HIGH);
      /* ---------------------------------------------
       * Check if we successfully decoded the message
       * ---------------------------------------------
       */
      if(!pb_decode(&istream, home_automation_msg_fields, &cmd_msg)) {
        //Serial.println("Error: !pb_decode(&istream, home_automation_msg_fields, &cmd_msg)");
        sendDataMessage(-2);
        digitalWrite(statusPIN3, HIGH);
      } else {
        /* ----------------------------------------
        * Decoding succeeded, process the message
        * ----------------------------------------
        */
        home_automation_msg resp_msg = {"c", cmd_msg.value};
    
        /* ---------------------------------------------------------------
         * Encode data message and forward it over serial
         * ---------------------------------------------------------------
         */
        bool python = false;
        if(python) {
          sendDataMessage(cmd_msg.value);
        }
        
        if(cmd_msg.value > 0) {
          digitalWrite(relayPIN, HIGH);
        } else {
          digitalWrite(relayPIN, LOW);      
        }
      }
    }
  }
  
  temp        = analogRead(tempPin);
  lightLevel1 = analogRead(photocellPin1);
  lightLevel2 = analogRead(photocellPin2);
  lightLevel3 = analogRead(photocellPin3);
  dat         = (125*temp)>>8 ; // Temperature calculation formula

  /* Send data every 1 second */  
  delay(50);
  timer++;
  
  if(timer == 20) {
    timer = 0;
    
    sendDataMessage(dat);
    delay(20);
    sendDataMessage(lightLevel1);
    delay(20);
  }
}

void sendDataMessage(int nodeId, double value) {
  home_automation_msg resp_msg = {"d", nodeId, value};
    
  /* ---------------------------------------------------------------
   * Encode data message and forward it over serial
   * ---------------------------------------------------------------
   */
  pb_ostream_t ostream = pb_ostream_from_buffer(out_buffer, sizeof(out_buffer));

  if (pb_encode(&ostream, home_automation_msg_fields, &resp_msg)) {
    if(xbee) {
      mySerial.write(ostream.bytes_written);
      mySerial.write(out_buffer, ostream.bytes_written);
    } else {
      Serial.write(ostream.bytes_written);
      Serial.write(out_buffer, ostream.bytes_written);
    }
  }
}
