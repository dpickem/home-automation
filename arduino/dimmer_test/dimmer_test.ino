/*

 AC Light Dimmer - Inmojo
 AC Voltage dimmer with Zero cross detection

 Author: Charith Fernanado http://www.inmojo.com charith@inmojo.com
 License: Released under the Creative Commons Attribution Share-Alike 3.0 License.
 http://creativecommons.org/licenses/by-sa/3.0
 Target:  Arduino

 Attach the Zero cross pin of the module to Arduino External Interrupt pin
 Select the correct Interrupt # from the below table

 Pin    |  Interrrupt # | Arduino Platform
 ---------------------------------------
 2      |  0            |  All
 3      |  1            |  All
 18     |  5            |  Arduino Mega Only
 19     |  4            |  Arduino Mega Only
 20     |  3            |  Arduino Mega Only
 21     |  2            |  Arduino Mega Only

 */

int AC_LOAD = 6;    // Output to Opto Triac pin
int dimming = 32;  // Dimming level (0-128)  0 = ON, 128 = OFF
//int dimtime = 65 * dimming;

void setup() {
  Serial.begin(115200);

  pinMode(AC_LOAD, OUTPUT);	      // Set the AC Load as output
  attachInterrupt(0, zero_crosss_int, RISING);  // Choose the zero cross interrupt # from the table above
  //attachInterrupt(0, zero_crosss_int, FALLING);  // Choose the zero cross interrupt # from the table above
  //attachInterrupt(0, zero_crosss_int, CHANGE);  // Choose the zero cross interrupt # from the table above

  Serial.println("Dimmer test started...");
}

void zero_crosss_int()  { // function to be fired at the zero crossing to dim the light
  // Firing angle calculation :: 60Hz-> 8.33ms (1/2 Cycle)
  // (8333us - 8.33us) / 128 = 65 (Approx)

  int dimtime = 65 * dimming;
  delayMicroseconds(dimtime);    // Off cycle
  digitalWrite(AC_LOAD, HIGH);   // triac firing
  delayMicroseconds(8);         // triac On propogation delay
  digitalWrite(AC_LOAD, LOW);    // triac Off

  //Serial.println("Zero crossing detected: ");
  //Serial.println(millis());
}

void loop() {
  dimming = 100;
  Serial.print("Set dimmer value to: "); Serial.println(dimming);
  delay(1000);
  dimming = 64;
  Serial.print("Set dimmer value to: "); Serial.println(dimming);
  delay(1000);
  dimming = 32;
  Serial.print("Set dimmer value to: "); Serial.println(dimming);
  delay(1000);
  dimming = 16;
  Serial.print("Set dimmer value to: "); Serial.println(dimming);
  delay(1000);
  dimming = 0;
  Serial.print("Set dimmer value to: "); Serial.println(dimming);
  delay(1000);
  dimming = 128;
  Serial.print("Set dimmer value to: "); Serial.println(dimming);
  
//  for (int dimming = 128; dimming > 0; dimming-=10) {
//    Serial.print("Set dimmer value to: "); Serial.println(dimming);
//    delay(1000);
//  }
}


  //  unsigned int ADCValue;
  //  double Voltage;
  //  double Vcc;
  //  Vcc = readVcc()/1000.0;
  //  ADCValue = analogRead(0);
  //  double m = (ADCValue / 1023.0) * Vcc;

  //  int m = analogRead(A0);
  //  if(m > 0) {
  //    dt = millis() - t_prev;
  //    t_prev = millis();
  //
  //    Serial.print(t_prev); Serial.print("/"); Serial.print(dt); Serial.print(": ");
  //    Serial.println(m);
  //
  //    t_prev = millis();
  //  }
  //
  //  delay(4);
  
//
//long readVcc() {
//  long result;
//
//  // Read 1.1V reference against AVcc
//  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
//  delay(2); // Wait for Vref to settle
//  ADCSRA |= _BV(ADSC); // Convert
//
//  while (bit_is_set(ADCSRA, ADSC));
//  result = ADCL;
//  result |= ADCH << 8;
//  result = 1125300L / result;
//
//  // Back-calculate AVcc in mV
//  return result;
//}
