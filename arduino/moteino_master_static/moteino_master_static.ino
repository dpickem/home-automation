// Sample RFM69 sender/node sketch, with ACK and optional encryption
// Sends periodic messages of increasing length to gateway (id=1)
// It also looks for an onboard FLASH chip, if present
// Library and code by Felix Rusu - felix@lowpowerlab.com
// Get the RFM69 and SPIFlash library at: https://github.com/LowPowerLab/

#include <RFM69.h>
#include <SPI.h>

/* Include nanopb interprocess communication */
#include <pb.h>
#include <pb_encode.h>
#include <pb_decode.h>

/* Include custom messages */
#include <home_automation_proto.pb.h>

#define NODEID         1    //unique for each node on same network
#define NETWORKID      100  //the same on all nodes that talk to each other
#define GATEWAYID      1

//Match frequency to the hardware version of the radio on your Moteino (uncomment one):
#define FREQUENCY       RF69_915MHZ         // #define FREQUENCY   RF69_433MHZ OR #define FREQUENCY   RF69_868MHZ
#define ENCRYPTKEY      "daniels&home"      //exactly the same 16 characters/bytes on all nodes!
#define ACK_TIME        30                  // max # of ms to wait for an ack
#define LED             9                   // Moteinos have LEDs on D9
#define SERIAL_BAUD     115200
#define TRANSMITPERIOD  1000              //transmit a packet to gateway so often (in ms)
#define RETRIES         3                 /* How often will a transmission be retried */
#define RETRY_WAIT_TIME 15                /* Wait time between retries */

/* Define message types */
#define MSG_ID_REQUEST  0
#define MSG_ID_GRANTED  1
#define MSG_SET_LIGHT   2
#define MSG_DATA        3

long lastSent        = millis();
bool promiscuousMode = true; /* set to 'true' to sniff all packets on the same network */
bool requestACK   = true;
bool debug = false;
RFM69 radio;

/* NanoPb message buffers */
uint8_t in_buffer[256];
uint8_t out_buffer[256];

/* Define payload struct */
typedef struct {		
  byte  nodeId;      /* store this nodeId */
  float current;     /* current consumption in amps               SLAVE -> MASTER*/
  float temp;        /* temperature in centigrade                 SLAVE -> MASTER*/
  int   lightLevel;  /* desired light level sent by master node   MASTER -> SLAVE */
  int   msgType;     /* indicate what type of message is being sent */
} Payload;
Payload data;

/* Function declarations */
void sendMessageNanoPb(int nodeId = -1, float lightLevel = -1, float current = -1, float temp = -1);

void setup() {
  /* Initialize serial and radio */
  Serial.begin(SERIAL_BAUD);
  radio.initialize(FREQUENCY, NODEID, NETWORKID);
  
  /* Enable hardware AES encryption */
  radio.encrypt(ENCRYPTKEY);
  
  if(debug) {
    Serial.print("Transmitting at "); Serial.print(FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915); Serial.println("  Mhz...");
  }
}

void loop() {
  /* ------------------------------------------- */
  /*         RECEIVE DATA FROM RFM69             */
  /* ------------------------------------------- */
  if (radio.receiveDone()) {
    if(debug) {
      Serial.print('[');Serial.print(radio.SENDERID, DEC);Serial.print("] ");
      Serial.print(" [RX_RSSI:");Serial.print(radio.readRSSI());Serial.print("]");
      if (promiscuousMode) {
        Serial.print("to [");Serial.print(radio.TARGETID, DEC);Serial.print("] ");
      }
    }
    
    /* Verify message length */
    if (radio.DATALEN != sizeof(Payload)) {
      //Serial.print("Invalid payload received, not matching Payload struct!");
    } else {
      /* Parse data - assume radio.DATA actually contains our struct and not something else */
      data = *(Payload*)radio.DATA; 
      
      if(debug) {
        Serial.print(" nodeId = ");     Serial.print(data.nodeId);
        Serial.print(" current = ");    Serial.print(data.current);
        Serial.print(" temp = ");       Serial.print(data.temp);
        Serial.print(" lightlevel = "); Serial.print(data.lightLevel);
        Serial.print(" msgType = ");    Serial.print(data.msgType);
      }
    }
    
    if (radio.ACK_REQUESTED) {
      radio.sendACK();
      
      if(debug) {
        Serial.println(" - ACK sent.");
      }
    } else {
      if(debug) {
        Serial.println();
      }
    }
    
    /* Process message */
    processMessage();
  }
  
  /* ------------------------------------------- */
  /*         RECEIVE DATA FROM SERIAL            */
  /* ------------------------------------------- */
  if(Serial.available()) {
    /* Read from software serial, since that is where the thermometer data is coming from */
    int packet_size = Serial.read();
    int bytes_read = Serial.readBytes((char*)in_buffer, packet_size);
    home_automation_msg msg;

    /* -------------------------------------------
     * Check if we read the right number of bytes
     * -------------------------------------------
     */
    if (bytes_read != packet_size) {
      /* ---------------------------------------------
       * ERROR: Wrong number of packets read
       * ---------------------------------------------
       */
      sendMessageNanoPb(-1);
    } else {
      pb_istream_t istream = pb_istream_from_buffer(in_buffer, bytes_read);

      /* ---------------------------------------------
       * Check if we successfully decoded the message
       * ---------------------------------------------
       */
      if(!pb_decode(&istream, home_automation_msg_fields, &msg)) {
        sendMessageNanoPb(-2);
      } else {
        /* ----------------------------------------
        * Decoding succeeded, process the message
        * ----------------------------------------
        */
        processMessageNanoPb(&msg);
      }
    }
  }
}

void sendMessageNanoPb(int nodeId, float lightLevel, float current, float temp) {
  home_automation_msg resp_msg = {"d", nodeId, lightLevel, current, temp};
    
  /* ---------------------------------------------------------------
   * Encode data message and forward it over serial
   * ---------------------------------------------------------------
   */
  pb_ostream_t ostream = pb_ostream_from_buffer(out_buffer, sizeof(out_buffer));

  if (pb_encode(&ostream, home_automation_msg_fields, &resp_msg)) {
    Serial.write(ostream.bytes_written);
    Serial.write(out_buffer, ostream.bytes_written);
  }
}

bool sendMessage(byte dest, int lightLevel, float current, float temp, int msgType) {
  /* Declare message */
  Payload tmpMsg;

  /* Fill in the struct with new values */
  tmpMsg.nodeId     = NODEID;
  tmpMsg.current    = current;
  tmpMsg.temp       = temp;
  tmpMsg.lightLevel = lightLevel;
  tmpMsg.msgType    = msgType;
  
//  Serial.print("sent nodeId = "); Serial.print(tmpMsg.nodeId);
//  Serial.print(" current = ");    Serial.print(tmpMsg.current);
//  Serial.print(" temp = ");       Serial.print(tmpMsg.temp);
//  Serial.print(" lightlevel = "); Serial.print(tmpMsg.lightLevel);
//  Serial.print(" msgType = ");    Serial.print(tmpMsg.msgType);
//  Serial.print(" dest = ");       Serial.println(dest);
    
  /* Send data if channel is not occupied */
  //Serial.print("Sending struct ("); Serial.print(sizeof(data)); Serial.print(" bytes) ... ");
  if(radio.canSend()) {
    if (radio.sendWithRetry(dest, (const void*)(&tmpMsg), sizeof(tmpMsg), RETRIES, RETRY_WAIT_TIME)) {
//      Serial.println("sendMessage: Message sent successfully.");
      return true;
    } else {
//      Serial.println("sendMessage: ERROR, no ACK received ...");
      return false;
    }
  }
}

void processMessageNanoPb(home_automation_msg* msg) {
  /* Send messge over RFM69 */
  sendMessage(msg->id, msg->lightLevel, msg->current, msg->temp, MSG_SET_LIGHT);
  
  /* TEST: send received message back over serial */
  //sendMessageNanoPb(msg->id, msg->lightLevel, msg->current, msg->temp);
}

void processMessage() {
  sendMessageNanoPb(data.nodeId, data.lightLevel, data.current, data.temp);
}
