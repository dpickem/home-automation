#include <SoftwareSerial.h>

int tempPin = 0 ;//define pin0 connect with LM35
int photocellPin1 = 1;
int photocellPin2 = 2;
int photocellPin3 = 3;
int buzzerPin     = 13;
int relayPin      = 12;

bool relay = false;

SoftwareSerial mySerial(2, 3); // RX, TX

void setup(){
  Serial.begin(9600);
  pinMode(buzzerPin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  
  mySerial.begin(9600);
}

void loop() {
  int temp;
  int lightLevel1, lightLevel2, lightLevel3;
  int dat;
  
  temp        = analogRead(tempPin);
  lightLevel1 = analogRead(photocellPin1);
  lightLevel2 = analogRead(photocellPin2);
  lightLevel3 = analogRead(photocellPin3);
  dat         = (125*temp)>>8 ; // Temperature calculation formula
  
//  Serial.print("Temp / Light Level : ") ; //print “Tep” means temperature
//  Serial.print(dat) ; // print the value of dat
//  Serial.print("C / ");
//  Serial.print(lightLevel1);
//  Serial.print(" / ");
//  Serial.print(lightLevel2);
//  Serial.print(" / ");
//  Serial.println(lightLevel3);
  
  mySerial.print("Temp / Light Level : ") ; //print “Tep” means temperature
  mySerial.print(dat) ; // print the value of dat
  mySerial.print("C / ");
  mySerial.print(lightLevel1);
  mySerial.print(" / ");
  mySerial.print(lightLevel2);
  mySerial.print(" / ");
  mySerial.println(lightLevel3);
  
  if(temp > 30) {
    digitalWrite(buzzerPin, HIGH); 
  } else {
    digitalWrite(buzzerPin, LOW); 
  }
  
  toggleRelay();
  delay(500);//delay 0.5s
}

void toggleRelay() {
  if(relay == false)  {
    digitalWrite(relayPin, HIGH);
    relay = true;
  } else {
    digitalWrite(relayPin, LOW);
    relay = false;
  }
}
