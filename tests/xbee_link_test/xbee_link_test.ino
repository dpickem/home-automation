/*
  XBee receiver test
 
 */
 
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3); // RX, TX

void setup()  
{
  // Open serial communications and wait for port to open:
  Serial.begin(9600);

  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  
  Serial.println("XBee receiver ready...");
}

void loop() {
  if (mySerial.available())
    Serial.write(mySerial.read());
}

