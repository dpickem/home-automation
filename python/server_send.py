#!/usr/bin/python

import serial
import sys

import home_automation_pb2

def main(argv):
	if len(argv) < 2:
		print 'Usage: server.py /dev/serial'
		return

	# Set up serial object
	ard_serial = serial.Serial(argv[1], 9600)
	print ard_serial.readline()

	while(True):
		cmd_msg = home_automation_pb2.msg()
		cmd_msg.type = 'c'
		cmd_msg.value = bool(input('Choose off/on (0 or 1): '))

		data = cmd_msg.SerializeToString()
		ard_serial.write('' + chr(len(data)))
		print 'Writing %d bytes to serial.' % len(data)
		ard_serial.write(data)

		# read while data is available but dont block commands
		for i in range(10):
			status_msg = home_automation_pb2.msg()
			msg_len = ard_serial.read(size=1)
			print 'Reading %d bytes from serial.' % ord(msg_len)
			try:
				status_msg.ParseFromString(ard_serial.read(ord(msg_len)))
			except:
				ard_serial.flushInput()
				print "Unexpected error:", sys.exc_info()[0]

			str = 'Received Echo with type: %s.' % status_msg.type
			str = str + ' and value: %d' % status_msg.value

			print str

if __name__ == '__main__':
  main(sys.argv)
