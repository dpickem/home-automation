#!/usr/bin/python

import serial
import sys

import home_automation_pb2

def main(argv):
	if len(argv) < 2:
		print 'Usage: server.py /dev/serial'
		return

	# Set up serial object
	ard_serial = serial.Serial(argv[1], 115200)
	#print ard_serial.readline()

	while(True):
		cmd_msg = home_automation_pb2.msg()
		cmd_msg.type = 'c'
		cmd_msg.current = -1
		cmd_msg.temp = -1
		cmd_msg.id = int(input('Choose node ID: '))
		cmd_msg.lightLevel = int(input('Choose light level: '))

		data = cmd_msg.SerializeToString()
		ard_serial.write('' + chr(len(data)))
		print 'Writing %d bytes to serial.' % len(data)
		ard_serial.write(data)
		print 'Written %d bytes to serial.' % len(data)

		# read while data is available but dont block commands
		for i in range(1):
			status_msg = home_automation_pb2.msg()
			msg_len = ard_serial.read(size=1)
			print 'Reading %d bytes from serial.' % ord(msg_len)
			try:
				status_msg.ParseFromString(ard_serial.read(ord(msg_len)))
			except:
				ard_serial.flushInput()
				print "Unexpected error:", sys.exc_info()[0]

			str = 'Received Echo with type: %s' % status_msg.type
			str = str + ' and id: %d' % status_msg.id
			str = str + ' and lightLevel: %d' % status_msg.lightLevel
			str = str + ' and current: %d' % status_msg.current
			str = str + ' and temp: %d' % status_msg.temp

			print str

if __name__ == '__main__':
  main(sys.argv)
