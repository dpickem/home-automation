#!/usr/bin/python

import serial
import sys
import threading
import time

import home_automation_pb2

connected = False
port = 'COM4'
baud = 9600

def serialRead(ser):
  while(True):
    status_msg = home_automation_pb2.msg()
    msg_len = ser.read(size=1)
    #print 'Reading %d bytes from serial.' % ord(msg_len)

    try:
      status_msg.ParseFromString(ser.read(ord(msg_len)))

      str = 'READ: Received Echo with type: %s' % status_msg.type
      str = str + ' and id: %d' % status_msg.id
      str = str + ' and lightLevel: %d' % status_msg.lightLevel
      str = str + ' and current: %d' % status_msg.current
      str = str + ' and temp: %d' % status_msg.temp

      print str
    except:
      ser.flushInput()
      print "Unexpected error:", sys.exc_info()[0]

def serialWrite(ser):
  lightLevel = 0
  increment  = 10

  # Instantiate message
  cmd_msg = home_automation_pb2.msg()
  cmd_msg.type = 'c'
  cmd_msg.current = -1
  cmd_msg.temp = -1
  cmd_msg.id = 2

  while(True):
    lightLevel += increment
    cmd_msg.lightLevel = lightLevel

    data = cmd_msg.SerializeToString()
    ser.write('' + chr(len(data)))
    ser.write(data)
    #print 'WRITE: Writing %d bytes to serial.' % len(data)

    # Make light pulsate
    if(lightLevel > 240 and increment > 0):
      increment = -10
    elif(lightLevel < 20 and increment < 0):
      increment = 10

    # Set frequency of update
    time.sleep(0.1)

def main(argv):
  if len(argv) < 2:
    print 'Usage: server.py /dev/serial'
    return

  # Set up serial object
  ard_serial = serial.Serial(argv[1], 115200)

  # Flush input queue
  ard_serial.flushInput()

  # Initialize threads
  threadRead  = threading.Thread(target = serialRead, args = (ard_serial,))
  threadWrite = threading.Thread(target = serialWrite, args = (ard_serial,))

  threadRead.start()
  threadWrite.start()

if __name__ == '__main__':
  main(sys.argv)
