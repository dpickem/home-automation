#!/usr/bin/python

import serial
import sys

import home_automation_pb2

from PyQt4 import QtCore, QtGui

class HomeAutomationGUI():
	def __init__(self, filename):
		self.data = []
		self.filename = filename
		self.width = 600
		self.height = 400
		self.posx = 100
		self.posy = 100

		self.app = QtGui.QApplication(sys.argv)
		self.window = QtGui.QMainWindow()
		self.window.setGeometry(self.posx, self.posy, self.width, self.height)
		

	def setImage(self, filename):
		self.pic = QtGui.QLabel(self.window)
		self.pic.setGeometry(5, 5, self.width-10, self.height-10)
		self.pixmap = QtGui.QPixmap(filename)
		self.pixmap = self.pixmap.scaledToHeight(self.height)
		self.pic.setPixmap(self.pixmap)

def main(argv):
	gui = HomeAutomationGUI("./maps/floor_map_7313.png")
	gui.setImage(gui.filename)

	gui.window.show()
	sys.exit(gui.app.exec_())

if __name__ == '__main__':
  main(sys.argv)
