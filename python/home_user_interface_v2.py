#!/usr/bin/python

import sys
import math
import json
import time
import random

# Import libraries for serial communication threads
import serial
import threading
from Queue import Queue

# Import messaging framework 
import home_automation_pb2

# Import Qt
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4.QtSvg import *
from PyQt4.QtGui import QInputDialog

#################################################################
# TODO: tie profiles to set lamps and store and load together
#		otherwise, profiles contain lamp locations that are not
#		otherwise set
#################################################################
class ThreadRead(threading.Thread):
  def __init__(self, in_queue, serial):
    self.serial = serial
    self.in_queue = in_queue
    #super(Worker, self).__init__()
    threading.Thread.__init__(self)

  def run(self):
    while(True):
      msg = home_automation_pb2.msg()
      msg_len = self.serial.read(size=1)

      try:
        msg.ParseFromString(self.serial.read(ord(msg_len)))

        data = {'type': msg.type, 'id': msg.id, 'lightLevel': msg.lightLevel, 'current': msg.current, 'temp': msg.temp}
        self.in_queue.put(data)
      except:
        self.serial.flushInput()

class ThreadWrite(threading.Thread):
  def __init__(self, out_queue, serial):
    self.serial = serial
    self.out_queue = out_queue      # This is just a pointer to the queue 
                                    # and can be filled from outside the thread
    threading.Thread.__init__(self)

  def run(self):
    while True:
      if (not self.out_queue.empty()):
        # Retrieve data item from out_queue
        data = self.out_queue.get()
        print 'THREADWRITE: ' + str(data)

        # Instantiate message
        cmd_msg = home_automation_pb2.msg()
        cmd_msg.type = data['type']
        cmd_msg.id = data['id']
        cmd_msg.lightLevel = data['lightLevel']
        cmd_msg.current = data['current']
        cmd_msg.temp = data['temp']

        # Write data to serial port
        data = cmd_msg.SerializeToString()
        self.serial.write('' + chr(len(data)))
        self.serial.write(data)

class HomeAutomation():
  def __init__(self, widget):
    self.parent = widget
    self.lamps = []
    self.profiles = {'kitchen': [], 'living room': [], 'bedroom': []} # this is a list of dictionaries
    self.setMode = False
    self.setProfile = False
    self.currentProfile = ''
    self.inputQueue = Queue(0)
    self.outputQueue = Queue(0)
    self.data = [{} for i in range(100)]    # initialize list of 100 dict to hold data for up to 100 nodes

    # Set up serial object
    self.ard_serial = serial.Serial('/dev/ttyUSB0', 115200)

    # Start thread
    # TODO: move this into home automation class
    try:
      self.threadRead   = ThreadRead(self.inputQueue, self.ard_serial).start()
      self.threadWrite  = ThreadWrite(self.outputQueue, self.ard_serial).start()
    except (KeyboardInterrupt, SystemExit):
      self.threadRead.stop()
      self.threadWrite.stop()

  # #########################################################
  # Data handling and update functions
  # #########################################################
  def updateData(self):
    # this function is called from the qt timer created in main window
    while(not self.inputQueue.empty()):
      # update data for every node and store in self.data list of dicts
      data = self.inputQueue.get() 
      self.data[data['id']] = data
      #print data

  # #########################################################
  # Button callback functions 
  # #########################################################
  def processMouseEvent(self, x, y):
    # If the setProfile button was activated, add lamps to profile
    # but do not allow to set new lamps
    if(self.setProfile):
      threshold = 20
      l = [x, y, False]

      for i in range(len(self.lamps)):
        if(self.dist(l, self.lamps[i]) < threshold):
          if(self.lamps[i] not in self.profiles[str(self.currentProfile)]):
            self.profiles[str(self.currentProfile)].append(self.lamps[i])
            print str(self.currentProfile) + ": " + str(self.profiles[str(self.currentProfile)])

      self.parent.drawProfile()
    else:
      if(self.setMode == True):
        self.addLamp(x, y)
        print "De-/activate lamps by pressing the button ''Set Lamps''"
      else:
        self.activateLamp(x, y)
        print "Add lamps by pressing the button ''Set Lamps''"

      # Redraw the lamps
      self.parent.drawLamps()

  def setLamps(self):
    if(not self.setProfile):
      if(self.setMode == False):
        self.setMode = True
        self.parent.btn_setLamps.setStyleSheet('QPushButton {color: black; background-color: #40FF00}')
      else:
        self.setMode = False
        self.parent.btn_setLamps.setStyleSheet('QPushButton {color: black; background-color: red}')

	# #########################################################
	# Functionality for adding, activating, and switching lamps
	# #########################################################
  def addLamp(self, x, y):
		threshold = 20
		append = True
		l = [x, y, False]

		for i in range(len(self.lamps)):
			if(self.dist(l, self.lamps[i]) < threshold):
				append = False

		if(append):
			self.lamps.append(l)
			print "addLamp: " + str(l[0]) + " / " + str(l[1]) + " / " + str(l[2])
		else:
			print "Lamp too close to already existant lamp"

		self.parent.drawLamps()
	
  def activateLamp(self, x, y):
		threshold = 20
		append = True
		l = [x, y, False]

		for i in range(len(self.lamps)):
			if(self.dist(l, self.lamps[i]) < threshold):
				self.switchLamp(i)
				append = False

		self.parent.drawLamps()
	
  def switchLamp(self, i):
		if(self.lamps[i][2] == True):
			self.lamps[i][2] = False
		else:
			self.lamps[i][2] = True

  def dist(self, l1, l2):
		#sqrt((x1 - x2)^2 + (y1 - y2)^2)
		return math.sqrt( math.pow(l1[0] - l2[0], 2) + math.pow(l1[1] - l2[1], 2))

class Main(QWidget):
  def __init__(self):
    super(Main, self).__init__()

    self.homeAutomation = HomeAutomation(self)

    hbox = QHBoxLayout()

    self.setLayout(hbox)
    self.view = MyView(self)
    self.scene = QGraphicsScene()
    self.view.setScene(self.scene)
    self.view.drawFloormap()

    hbox.addWidget(self.view)

    # Define button locations
    self.column1 = 100
    self.column2 = self.column1 + 100
    self.column3 = self.column2 + 100

    # Define buttons for setting, saving, loading lamps
    self.btn_setLamps = QtGui.QPushButton('Set Lamps', self)
    self.btn_setLamps.resize(100, 30)
    self.btn_setLamps.move(self.column1, 30)
    self.btn_setLamps.clicked.connect(self.homeAutomation.setLamps)
    self.btn_setLamps.setStyleSheet('QPushButton {color: black; background-color: red}')

    # Create label for testing timer
    self.lbl_node = QtGui.QLabel(self)
    self.lbl_node.setText('0')
    self.lbl_node.setGeometry(self.column1, 60, 100, 30)
    self.lbl_node.setAlignment(QtCore.Qt.AlignCenter)
    self.lbl_node.setStyleSheet('QLabel {color: black; background-color: red}')

    self.lbl_temp = QtGui.QLabel(self)
    self.lbl_temp.setText('0')
    self.lbl_temp.setGeometry(self.column1, 90, 100, 30)
    self.lbl_temp.setAlignment(QtCore.Qt.AlignCenter)
    self.lbl_temp.setStyleSheet('QLabel {color: black; background-color: red}')

    # Create labels for every node
    self.node_labels = [{} for i in range(100)]    # initialize list of 100 dict to hold data for up to 100 nodes
    for i in range(10):
      tmp = {'label_id': QGraphicsSimpleTextItem(""), 'label_light': QGraphicsSimpleTextItem(""),
          'label_temp': QGraphicsSimpleTextItem(""), 'label_current': QGraphicsSimpleTextItem(""),
          'x': 0, 'y': 0}

      # Set font color and bg color
      tmp['label_id'].setBrush(QtCore.Qt.red)
      tmp['label_light'].setBrush(QtCore.Qt.red)
      tmp['label_temp'].setBrush(QtCore.Qt.red)
      tmp['label_current'].setBrush(QtCore.Qt.red)

      # Add tmp data structure to node_labels
      self.node_labels[i] = tmp

      # Add labels to scene
      self.scene.addItem(tmp['label_id'])
      self.scene.addItem(tmp['label_light'])
      self.scene.addItem(tmp['label_temp'])
      self.scene.addItem(tmp['label_current'])

    self.node_labels[2]['x'] = 130
    self.node_labels[2]['y'] = 200
    self.node_labels[3]['x'] = 400
    self.node_labels[3]['y'] = 160
    self.node_labels[4]['x'] = 200
    self.node_labels[4]['y'] = 360

    # Create time for data updates 
    self.timerRead  = QtCore.QTimer()
    self.connect(self.timerRead, SIGNAL('timeout()'), self.on_timerRead)    # this signal would be emitted by the read thread

    # check every second
    self.timerRead.start(1000)

  def on_timerRead(self):
    """ Executed periodically when the monitor update timer is fired.  """
    self.homeAutomation.updateData()
    self.updateLabels()

    # Add outgoing messages to make node 2 pulse
    data = {'type': 'c', 'id': 2, 'lightLevel': random.randint(0, 255), 'current': -1, 'temp': -1}
    self.homeAutomation.outputQueue.put(data)

  def updateLabels(self):
    for i in range(100):
      if(self.homeAutomation.data[i]):
        self.drawNode(self.homeAutomation.data[i], i)

    #data = self.homeAutomation.inputQueue.get()
    data = self.homeAutomation.data[2]
    print data

    if(data):
      node = data['id']
      temp = data['temp']
      
      #data = {'type': msg.type, 'id': msg.id, 'lightLevel': msg.lightLevel, 'current': msg.current, 'temp': msg.temp}
      self.lbl_node.setText(str(node))
      self.lbl_temp.setText("{:2.3f}".format(temp))

      #self.drawNode(data, (130, 60)) 

  def drawNode(self, data, i):
    self.view.drawNode(data, i)

  def drawLamps(self):
		self.view.drawLamps()

class MyView(QGraphicsView):
  def __init__(self, parent):
    super(MyView, self).__init__(parent)
    self.parent = parent
	
  def drawFloormap(self):
    test = MySvg(self.parent)
    self.parent.scene.addItem(test)
	
  def drawLamps(self):
    lamps = self.parent.homeAutomation.lamps

    for i in range(len(lamps)):
      ellipse = QGraphicsEllipseItem(float(lamps[i][0]),float(lamps[i][1]), 10, 10)

      # Draw active lamps in green, inactive ones in red
      if(lamps[i][2]):
        ellipse.setPen(QtCore.Qt.green)
        ellipse.setBrush(QtCore.Qt.green)
      else:
        ellipse.setPen(QtCore.Qt.red)
        ellipse.setBrush(QtCore.Qt.red)

      self.parent.scene.addItem(ellipse)

  def drawNode(self, data, i):
    # Define ellips
    x = self.parent.node_labels[i]['x']
    y = self.parent.node_labels[i]['y']
    ellipse = QGraphicsEllipseItem(float(x),float(y), 10, 10)
    #ellipse = QGraphicsEllipseItem(float(pos[0]),float(pos[1]), 10, 10)

    # Set color of ellipse
    lightLevel = int(data['lightLevel'])
    #ellipse.setPen(QtCore.Qt.green)
    #ellipse.setBrush(QtCore.Qt.green)
    ellipse.setPen(QColor(0, lightLevel, 0))
    ellipse.setBrush(QColor(0, lightLevel, 0))

    # Draw ellipse
    self.parent.scene.addItem(ellipse)

    # TODO: instead of drawing over previous label, store labels in main window class
    #       and update the text in the labels with QGraphicsSimpleTextItem.setText (self, QString text)

    # Add labels
    self.parent.node_labels[i]['label_id'].setText("{:2.3f}".format(data['id']))
    self.parent.node_labels[i]['label_light'].setText("{:2.3f}".format(data['lightLevel']))
    self.parent.node_labels[i]['label_temp'].setText("{:2.3f}".format(data['temp']))
    self.parent.node_labels[i]['label_current'].setText("{:2.3f}".format(data['current']))

    #label_id      = QGraphicsSimpleTextItem("{:2.3f}".format(data['id']))
    #label_light   = QGraphicsSimpleTextItem("{:2.3f}".format(data['lightLevel']))
    #label_temp    = QGraphicsSimpleTextItem("{:2.3f}".format(data['temp']))
    #label_current = QGraphicsSimpleTextItem("{:2.3f}".format(data['current']))
        
    # Set position of labels
    self.parent.node_labels[i]['label_id'].setPos(float(x + 15), float(y - 30))
    self.parent.node_labels[i]['label_light'].setPos(float(x + 15), float(y - 10))
    self.parent.node_labels[i]['label_temp'].setPos(float(x + 15), float(y + 10))
    self.parent.node_labels[i]['label_current'].setPos(float(x + 15), float(y + 30))

    #label_id.translate(float(pos[0] + 10),float(pos[1] - 20))
    #label_light.translate(float(pos[0] + 10),float(pos[1]))
    #label_temp.translate(float(pos[0] + 10),float(pos[1] + 20))
    #label_current.translate(float(pos[0] + 10),float(pos[1] + 40))

    # Add labels to scene
    #self.parent.scene.addItem(label_id)
    #self.parent.scene.addItem(label_light)
    #self.parent.scene.addItem(label_temp)
    #self.parent.scene.addItem(label_current)

  def mousePressEvent(self, event):
    super(MyView, self).mousePressEvent(event)

    print "Mouse click at " + str(event.x()) + " / " + str(event.y())
    print "Button pressed: " + str(event.button())

    if(event.button() == 2):
      sys.exit()

class MySvg(QGraphicsSvgItem):
	def __init__(self, parent):
		super(MySvg, self).__init__('./maps/floor_map_7313.svg')

		#self.setFlags(QGraphicsItem.ItemIsSelectable| QGraphicsItem.ItemIsMovable)
		self.setFlags(QGraphicsItem.ItemIsSelectable)
		self.setAcceptsHoverEvents(True)
		self.parent = parent

	def mousePressEvent(self, event):
		# exit if the right mouse button was clicked
		if(event.button() == 2):
			sys.exit()

		# event has fields of QGraphicsSceneMouseEvent (see 
		# http://pyqt.sourceforge.net/Docs/PyQt4/qgraphicsscenemouseevent.html), e.g. pos and scenePos
		self.parent.homeAutomation.processMouseEvent(event.pos().x(), event.pos().y())

def runMain():
	app = QApplication(sys.argv)
	ex = Main()
	ex.show()
	sys.exit(app.exec_())

if __name__ == '__main__':
	runMain()
