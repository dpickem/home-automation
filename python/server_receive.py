#!/usr/bin/python

import serial
import sys

import home_automation_pb2

def main(argv):
	if len(argv) < 2:
		print 'Usage: server.py /dev/serial'
		return

	# Set up serial object
	ard_serial = serial.Serial(argv[1], 9600)
	print ard_serial.readline()


	for i in range(100):
		status_msg = home_automation_pb2.msg()
		msg_len = ard_serial.read(size=1)
		print 'Reading %d bytes from serial.' % ord(msg_len)

		try:
			status_msg.ParseFromString(ard_serial.read(ord(msg_len)))
		except:
			ard_serial.flushInput()
			print "Unexpected error:", sys.exc_info()[0]

		str = 'Received Echo with type: %s.' % status_msg.type
		str = str + ' and value: %d' % status_msg.value

		print str

if __name__ == '__main__':
  main(sys.argv)
