#!/usr/bin/python

import sys
import math
import json
import time

# Import libraries for serial communication threads
import serial
import threading
from Queue import Queue

# Import messaging framework 
import home_automation_pb2

# Import Qt
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4.QtSvg import *
from PyQt4.QtGui import QInputDialog

#################################################################
# TODO: tie profiles to set lamps and store and load together
#		otherwise, profiles contain lamp locations that are not
#		otherwise set
#################################################################
class ThreadRead(threading.Thread):
  def __init__(self, out_queue, serial):
    self.serial = serial
    self.out_queue = out_queue
    #super(Worker, self).__init__()
    threading.Thread.__init__(self)

  def run(self):
    while(True):
      msg = home_automation_pb2.msg()
      msg_len = self.serial.read(size=1)

      try:
        msg.ParseFromString(self.serial.read(ord(msg_len)))

        data = {'type': msg.type, 'id': msg.id, 'lightLevel': msg.lightLevel, 'current': msg.current, 'temp': msg.temp}
        print data
      except:
        self.serial.flushInput()

class ThreadWrite(threading.Thread):
  def __init__(self, in_queue, serial):
    self.serial = serial
    self.out_queue = out_queue      # This is just a pointer to the queue 
                                    # and can be filled from outside the thread
    super(Worker, self).__init__()

  def run(self):
    while True:
      if (self.in_queue.empty()):
        break
      #else:
        #data = out_queue.get()
        #result = self.function(*data)
        #self.out_queue.put(result)
        #self.in_queue.task_done()

class HomeAutomation():
  def __init__(self, widget):
    self.parent = widget
    self.lamps = []
    self.profiles = {'kitchen': [], 'living room': [], 'bedroom': []} # this is a list of dictionaries
    self.setMode = False
    self.setProfile = False
    self.currentProfile = ''

    # Set up serial object
    self.ard_serial = serial.Serial('/dev/ttyUSB0', 115200)

    # Start thread
    # TODO: move this into home automation class
    try:
      self.threadRead = ThreadRead(Queue(0), self.ard_serial).start()
    except (KeyboardInterrupt, SystemExit):
      self.threadRead.stop()

  # #########################################################
  # Button callback functions 
  # #########################################################
  def processMouseEvent(self, x, y):
    # If the setProfile button was activated, add lamps to profile
    # but do not allow to set new lamps
    if(self.setProfile):
      threshold = 20
      l = [x, y, False]

      for i in range(len(self.lamps)):
        if(self.dist(l, self.lamps[i]) < threshold):
          if(self.lamps[i] not in self.profiles[str(self.currentProfile)]):
            self.profiles[str(self.currentProfile)].append(self.lamps[i])
            print str(self.currentProfile) + ": " + str(self.profiles[str(self.currentProfile)])

      self.parent.drawProfile()
    else:
      if(self.setMode == True):
        self.addLamp(x, y)
        print "De-/activate lamps by pressing the button ''Set Lamps''"
      else:
        self.activateLamp(x, y)
        print "Add lamps by pressing the button ''Set Lamps''"

      # Redraw the lamps
      self.parent.drawLamps()

  def setLamps(self):
    if(not self.setProfile):
      if(self.setMode == False):
        self.setMode = True
        self.parent.btn_setLamps.setStyleSheet('QPushButton {color: black; background-color: #40FF00}')
      else:
        self.setMode = False
        self.parent.btn_setLamps.setStyleSheet('QPushButton {color: black; background-color: red}')

  def createLightingProfile(self):
    if(not self.setMode): 
      if(self.setProfile == False):
        # Activate edit mode
        self.setProfile = True
        self.parent.btn_createProfile.setStyleSheet('QPushButton {color: black; background-color: #40FF00}')

        # Read in name for new profile 
        (text, truth) = QInputDialog.getText(self.parent, "Get text", "Profile name", QLineEdit.Normal, "default")

        if(text in self.profiles.keys()):
          print "profile " + text + " already existant profile, appending to profile"
          print self.profiles[str(text)]
        else:
          # create new profile
          self.profiles[str(text)] = []

          # add to combobox items
          self.parent.combo_lightingProfiles.addItem(str(text))

        self.currentProfile = text
        self.parent.drawProfile()
      else:
        self.setProfile = False
        self.parent.btn_createProfile.setStyleSheet('QPushButton {color: black; background-color: red}')
        self.parent.drawLamps()

  def setCurrentProfile(self):
		# Redraw lamps
		self.parent.drawLamps()

		# Retrieve currently selected element from combobox
		index = self.parent.combo_lightingProfiles.currentIndex()
		profile = self.parent.combo_lightingProfiles.itemText(index)

		# If a valid profile is selected, set it as current profile and draw it
		if profile in self.profiles.keys():
			self.currentProfile = profile
			self.parent.drawProfile()
	
  def getCurrentProfile(self):
		return self.profiles[str(self.currentProfile)]
	
	# #########################################################
	# File I/O functionality for saving / loading lamps
	# #########################################################
  def saveLamps(self):
		fileName = QtGui.QFileDialog.getSaveFileName(self.parent, 'Save File', '.lmp', '*.lmp')

		if(len(fileName) > 0):
			fileHandle = open(fileName, 'w')

			for i in range(len(self.lamps)):
				s = str(self.lamps[i][0]) + " " + str(self.lamps[i][1]) + "\n"
				fileHandle.write(s)

			fileHandle.close()

  def loadLamps(self):
		fileName = QtGui.QFileDialog.getOpenFileName(self.parent, 'Load File', '.lmp', '*.lmp')

		if(len(fileName) > 0):
			fileHandle = open(fileName, 'r')

			lines = fileHandle.read().splitlines()

			self.lamps = []
			for i in range(len(lines)):
				s = lines[i].split()
				l = [float(s[0]), float(s[1]), False]
				self.lamps.append(l)

			fileHandle.close()

		self.parent.drawLamps()

	# #########################################################
	# File I/O functionality for saving / loading profiles 
	# #########################################################
  def saveProfiles(self):
		fileName = QtGui.QFileDialog.getSaveFileName(self.parent, 'Save Profiles', '.json', '*.json')

		if(len(fileName) > 0):
			with open(fileName, 'wb') as fp:
			    json.dump(self.profiles, fp)


  def loadProfiles(self):
		fileName = QtGui.QFileDialog.getOpenFileName(self.parent, 'Load Profiles', '.json', '*.json')

		if(len(fileName) > 0):
			with open(fileName, 'rb') as fp:
				    self.profiles = json.load(fp)

			print self.profiles

			keys = self.profiles.keys()
			self.parent.combo_lightingProfiles.clear()

			for i in range(len(keys)):
				self.parent.combo_lightingProfiles.addItem(keys[i])

	# #########################################################
	# Functionality for adding, activating, and switching lamps
	# #########################################################
  def addLamp(self, x, y):
		threshold = 20
		append = True
		l = [x, y, False]

		for i in range(len(self.lamps)):
			if(self.dist(l, self.lamps[i]) < threshold):
				append = False

		if(append):
			self.lamps.append(l)
			print "addLamp: " + str(l[0]) + " / " + str(l[1]) + " / " + str(l[2])
		else:
			print "Lamp too close to already existant lamp"

		self.parent.drawLamps()
	
  def activateLamp(self, x, y):
		threshold = 20
		append = True
		l = [x, y, False]

		for i in range(len(self.lamps)):
			if(self.dist(l, self.lamps[i]) < threshold):
				self.switchLamp(i)
				append = False

		self.parent.drawLamps()
	
  def switchLamp(self, i):
		if(self.lamps[i][2] == True):
			self.lamps[i][2] = False
		else:
			self.lamps[i][2] = True

  def dist(self, l1, l2):
		#sqrt((x1 - x2)^2 + (y1 - y2)^2)
		return math.sqrt( math.pow(l1[0] - l2[0], 2) + math.pow(l1[1] - l2[1], 2))

class Main(QWidget):
  def __init__(self):
    super(Main, self).__init__()

    self.homeAutomation = HomeAutomation(self)

    hbox = QHBoxLayout()

    self.setLayout(hbox)
    self.view = MyView(self)
    self.scene = QGraphicsScene()
    self.view.setScene(self.scene)
    self.view.drawFloormap()

    hbox.addWidget(self.view)

    # Define button locations
    self.column1 = 400
    self.column2 = self.column1 + 100
    self.column3 = self.column2 + 100
    # Define buttons for setting, saving, loading lamps
    self.btn_setLamps = QtGui.QPushButton('Set Lamps', self)
    self.btn_setLamps.resize(100, 30)
    self.btn_setLamps.move(self.column1, 30)
    self.btn_setLamps.clicked.connect(self.homeAutomation.setLamps)
    self.btn_setLamps.setStyleSheet('QPushButton {color: black; background-color: red}')

    self.btn_moveLamps = QtGui.QPushButton('Move Lamps', self)
    self.btn_moveLamps.resize(100, 30)
    self.btn_moveLamps.move(self.column1, 60)
    self.btn_moveLamps.clicked.connect(self.homeAutomation.saveLamps)
    #self.btn_moveLamps.setStyleSheet('QPushButton {color: black; background-color: red}')

    self.btn_saveLamps = QtGui.QPushButton('Save Lamps', self)
    self.btn_saveLamps.resize(100, 30)
    self.btn_saveLamps.move(self.column1, 90)
    self.btn_saveLamps.clicked.connect(self.homeAutomation.saveLamps)

    self.btn_loadLamps = QtGui.QPushButton('Load Lamps', self)
    self.btn_loadLamps.resize(100, 30)
    self.btn_loadLamps.move(self.column1, 120)
    self.btn_loadLamps.clicked.connect(self.homeAutomation.loadLamps)

    # Define edit fields, buttons, and combobox for adding light profiles
    self.btn_createProfile = QtGui.QPushButton('Create Profile', self)
    self.btn_createProfile.resize(100, 30)
    self.btn_createProfile.move(self.column2, 30)
    self.btn_createProfile.clicked.connect(self.homeAutomation.createLightingProfile)
    self.btn_createProfile.setStyleSheet('QPushButton {color: black; background-color: red}')

    self.btn_saveProfiles = QtGui.QPushButton('Save Profiles', self)
    self.btn_saveProfiles.resize(100, 30)
    self.btn_saveProfiles.move(self.column2, 60)
    self.btn_saveProfiles.clicked.connect(self.homeAutomation.saveProfiles)

    self.btn_loadProfiles = QtGui.QPushButton('Load Profiles', self)
    self.btn_loadProfiles.resize(100, 30)
    self.btn_loadProfiles.move(self.column2, 90)
    self.btn_loadProfiles.clicked.connect(self.homeAutomation.loadProfiles)

    self.combo_lightingProfiles = QtGui.QComboBox(self) 
    self.combo_lightingProfiles.resize(100, 30)
    self.combo_lightingProfiles.move(self.column3, 30)
    self.combo_lightingProfiles.activated.connect(self.homeAutomation.setCurrentProfile)

  def drawProfile(self):
    self.view.drawProfile()

  def drawLamps(self):
		self.view.drawLamps()

class MyView(QGraphicsView):
	def __init__(self, parent):
		super(MyView, self).__init__(parent)
		self.parent = parent
	
	def drawFloormap(self):
		test = MySvg(self.parent)
		self.parent.scene.addItem(test)
	
	def drawProfile(self):
		profile = self.parent.homeAutomation.getCurrentProfile()

		for i in range(len(profile)):
			ellipse = QGraphicsEllipseItem(float(profile[i][0]),float(profile[i][1]), 10, 10)

			# Draw active lamps in green, inactive ones in red
			ellipse.setPen(QtCore.Qt.blue)
			ellipse.setBrush(QtCore.Qt.blue)

			self.parent.scene.addItem(ellipse)

	def drawLamps(self):
		lamps = self.parent.homeAutomation.lamps

		for i in range(len(lamps)):
			ellipse = QGraphicsEllipseItem(float(lamps[i][0]),float(lamps[i][1]), 10, 10)

			# Draw active lamps in green, inactive ones in red
			if(lamps[i][2]):
				ellipse.setPen(QtCore.Qt.green)
				ellipse.setBrush(QtCore.Qt.green)
			else:
				ellipse.setPen(QtCore.Qt.red)
				ellipse.setBrush(QtCore.Qt.red)

			self.parent.scene.addItem(ellipse)

	#def mousePressEvent(self, event):
		#super(MyView, self).mousePressEvent(event)

		#print "Mouse click at " + str(event.x()) + " / " + str(event.y())
		#print "Button pressed: " + str(event.button())

		#if(event.button() == 2):
			#sys.exit()

class MySvg(QGraphicsSvgItem):
	def __init__(self, parent):
		super(MySvg, self).__init__('./maps/floor_map_7313.svg')

		#self.setFlags(QGraphicsItem.ItemIsSelectable| QGraphicsItem.ItemIsMovable)
		self.setFlags(QGraphicsItem.ItemIsSelectable)
		self.setAcceptsHoverEvents(True)
		self.parent = parent

	def mousePressEvent(self, event):
		# exit if the right mouse button was clicked
		if(event.button() == 2):
			sys.exit()

		# event has fields of QGraphicsSceneMouseEvent (see 
		# http://pyqt.sourceforge.net/Docs/PyQt4/qgraphicsscenemouseevent.html), e.g. pos and scenePos
		self.parent.homeAutomation.processMouseEvent(event.pos().x(), event.pos().y())

def runMain():
	app = QApplication(sys.argv)
	ex = Main()
	ex.show()
	sys.exit(app.exec_())

if __name__ == '__main__':
	runMain()
